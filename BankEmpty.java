package bankomat.state;

public class BankEmpty implements BankState{
	
	@Override
	public void doAction(Bank b) {
		b.setBlocked(true);
		System.out.println("Bank is empty you cannot do any actions");
	}
	
}
