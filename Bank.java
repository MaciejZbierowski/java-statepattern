package bankomat.state;

public class Bank {

	private int status;
	private boolean blocked = true;
	private int insertedPin;
	//dowiedziec sie czy taka zmienna reprezentujaca PIN moze byc prywatna
	private int pin = 1234;
	private int payment;
	private boolean payingCard = false;
	
	public Bank (boolean payingCard, int pin, int payment){
		this.payingCard = payingCard;
		this.insertedPin = pin;
		this.payment = payment;
	}
	
	public boolean checkIfPinEqualsInsertedPin(){
		return pin == insertedPin;
	}
	
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isBlocked() {
		return blocked;
	}
	
	public int getInsertedPin() {
		return insertedPin;
	}
	
	public int getPin() {
		return pin;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getPayment() {
		return payment;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setPayment(int payment) {
		this.payment = payment;
	}
	
	public void setInsertedPin(int pin) {
		this.insertedPin = pin;
	}
	
	public boolean isPayingCard() {
		return payingCard;
	}
}
