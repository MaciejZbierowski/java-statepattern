package bankomat.state;

public class BankFull implements BankState {

	@Override
	public void doAction(Bank b) {
		b.setStatus(2000);
		b.setBlocked(false);
		System.out.println("Bank is full, you can withdraw money.");
	}

}