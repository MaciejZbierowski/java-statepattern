package bankomat.state;

public class BankContext implements BankState{

	private BankState state;
	
	public void setState(BankState state) {
		this.state = state;
	}
	
	@Override
	public void doAction(Bank b) {
		this.state.doAction(b);
	}
}
