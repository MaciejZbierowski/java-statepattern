package bankomat.state;

public class Testing {

	public static void main(String[] args) {
		
		Bank bank = new Bank(true, 1234, 200);
		
		BankContext bankContext = new BankContext();
		BankState bankFull = new BankFull();
		BankState bankEmpty = new BankEmpty();
		BankState witMoney = new BankWithdrawMoney(); 
		
		bankContext.setState(bankFull);
		bankContext.doAction(bank);
		
		bankContext.setState(witMoney);
		bankContext.doAction(bank);
		
		bankContext.setState(bankEmpty);
		bankContext.doAction(bank);
		
		bankContext.setState(witMoney);
		bankContext.doAction(bank);
		
	}
}
