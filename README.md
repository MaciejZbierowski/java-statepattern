Bankomat
Twoim zadaniem jest przygotowanie oprogramowania do bankomatów banku „Szmallenium”.
Bankomat nie ma być aktywny gdy nikt nie użył karty płatniczej. Jeśli karta płatnicza została użyta,
bankomat prosi o wpisanie pinu do karty. Jeśli pin trzykrotnie został wprowadzony błędnie to
bankomat zablokuje kartę i nie będzie można już jej więcej użyć. Po poprawnym wprowadzeniu pinu
– ilość nieudanych prób wpisania pinu zostaje zresetowania do zera, a klient może przystąpić do
wypłaty środków (zakładamy, że klient posiada wystarczające środki na swoim koncie) – wtedy
bankomat sprawdza, czy posiada w sejfie wystarczającą ilość gotówki, jeśli posiada finalizuje
transkację, jeśli nie posiada wyastarczjącej ilości gotówki to informuje o tym klienta i proponuje mu
wypłatę w niższej kwocie. Jeśli bankomat nie posiada w sejfie gotówki to zostaje zablokowany i do
czasu uzupełnienia pieniędzy żaden klient nie będzie mógł z niego korzystać.
Wytyczne zadania:
 Wykorzystaj wzorzec projektowy „stan” (state pattern) do rozwiązania zadania