package bankomat.state;

public class BankWithdrawMoney implements BankState {

	private int licznik = 0;
	
	@Override
	public void doAction(Bank b) {
		
		//if( !b.isPayingCard()){
		if(!b.isBlocked() && b.isPayingCard()){
			if(!(b.checkIfPinEqualsInsertedPin())){
				System.out.println("Nieprawidlowy pin, sprobuj ponownie");
				licznik++;
				if(licznik == 3){
					b.setBlocked(true);
					System.out.println("Zablokowany bankomat");
				}
			}
			else {
				System.out.println("PIN ok");
				licznik = 0;
				if(b.getPayment() <= b.getStatus()){
					b.setStatus(b.getStatus() - b.getPayment());
					System.out.println("Mozna wyplacac. Wyplacono : " + b.getPayment() + ", pozostalo: " + b.getStatus());
				}
				else if (b.getStatus() == 0)
					System.out.println("Nie ma kasy.");
				else if(b.getPayment() > b.getStatus())
					System.out.println("Nie ma tyle kasy w bankomacie, proponuje " + (b.getStatus() - b.getStatus()*0.25));
			}
		} else System.out.println("Bankomat zablokowany lub nie wlozono prawidlowej karty");
		
	}

}
